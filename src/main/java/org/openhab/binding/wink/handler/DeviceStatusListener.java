package org.openhab.binding.wink.handler;

import org.openhab.binding.wink.model.WinkDevice;

public interface DeviceStatusListener {

    public void onDeviceFound(WinkDevice d);

    public void onDeviceRemoved(WinkDevice d);

    public void onDeviceStateChanged(WinkDevice light);

}
