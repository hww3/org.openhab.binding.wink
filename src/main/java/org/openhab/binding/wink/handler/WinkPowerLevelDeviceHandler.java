/**
 * Copyright (c) 2014-2016 by the respective copyright holders.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.wink.handler;

import org.eclipse.smarthome.core.library.types.IncreaseDecreaseType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.library.types.PercentType;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.thing.ThingStatusInfo;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.openhab.binding.wink.WinkBindingConstants;
import org.openhab.binding.wink.internal.StateUpdate;
import org.openhab.binding.wink.model.WinkDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link WinkPowerLevelDeviceHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Welliver - Initial contribution
 */
public class WinkPowerLevelDeviceHandler extends BaseThingHandler implements DeviceStatusListener {

    protected Logger log = LoggerFactory.getLogger(getClass());

    protected int deviceId;
    protected WinkDevice device; // last update received for this device.

    protected WinkHandler hubHandler;

    protected final String powerLevelChannelName;
    // protected final String powerSwitchChannelName;

    public WinkPowerLevelDeviceHandler(Thing thing, String powerLevelChannel, String powerSwitchChannel) {
        super(thing);
        this.powerLevelChannelName = powerLevelChannel;
        // this.powerSwitchChannelName = powerSwitchChannel;
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        StateUpdate lightState = null;
        String ch = channelUID.getId();
        log.warn("Got a command for channel id=" + ch);
        if (log.isTraceEnabled()) {
            log.trace("command= " + command + ", last device reading=" + getDevice().getLastReading());
        }
        if (powerLevelChannelName.equals(ch)) {
            if (command instanceof PercentType) {
                lightState = LightStateConverter.toBrightnessLightState((PercentType) command);
            } else if (command instanceof OnOffType) {
                lightState = LightStateConverter.toOnOffLightState((OnOffType) command, getDevice());
            } else if (command instanceof IncreaseDecreaseType) {
                lightState = convertLevelChangeToStateUpdate((IncreaseDecreaseType) command, getDevice());
            }
        }

        /*
         * else if (powerSwitchChannelName.equals(ch)) {
         * if (command instanceof OnOffType) {
         * lightState = LightStateConverter.toOnOffLightState((OnOffType) command, getDevice());
         * }
         * }
         */

        if (lightState != null) {
            if (log.isTraceEnabled()) {
                log.trace("converted " + command + " to " + lightState.toJson());
            }
            updateDeviceState(lightState);
        } else {
            log.warn("Got a command for an unhandled channel: " + ch);
        }
    }

    protected void updateDeviceState(StateUpdate lightState) {
        getWinkHandler().setLightDesiredState(deviceId, lightState);
    }

    @Override
    public void initialize() {
        log.debug("Initializing wink power level device handler.");
        initializeThing((getBridge() == null) ? null : getBridge().getStatus());
    }

    private void initializeThing(ThingStatus bridgeStatus) {
        log.debug("initializeThing thing {} bridge status {}", getThing().getUID(), bridgeStatus);
        final String configDeviceId = getThing().getProperties().get(WinkBindingConstants.PROPERTY_OBJECT_ID);
        if (configDeviceId != null) {
            deviceId = Integer.valueOf(configDeviceId);

            if (getWinkHandler() != null) {
                if (bridgeStatus == ThingStatus.ONLINE) {
                    WinkDevice device = getWinkHandler().getLightDeviceById(deviceId);
                    if (device == null) {
                        updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.BRIDGE_OFFLINE);
                        return;
                    }
                    updateStatus(ThingStatus.ONLINE);
                    onDeviceStateChanged(getWinkHandler().getLightDeviceById(deviceId));
                    // initializeProperties();
                } else {
                    updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.BRIDGE_OFFLINE);
                }
            } else {
                updateStatus(ThingStatus.OFFLINE);
            }
        } else {
            updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR);
        }
    }

    @Override
    public void dispose() {
        log.debug("Handler disposed. Unregistering listener.");
        if (deviceId != 0) { // technically 0 is a valid device id but it appears to be reserved for the hub
            WinkHandler hubHandler = getWinkHandler();
            if (hubHandler != null) {
                hubHandler.unregisterDeviceStatusListener(this);
                this.hubHandler = null;
            }
            deviceId = 0;
        }
    }

    protected synchronized WinkHandler getWinkHandler() {
        if (this.hubHandler == null) {
            Bridge bridge = getBridge();
            if (bridge == null) {
                return null;
            }
            ThingHandler handler = bridge.getHandler();
            if (handler instanceof WinkHandler) {
                this.hubHandler = (WinkHandler) handler;
                this.hubHandler.registerDeviceStatusListener(this);
            } else {
                return null;
            }
        }
        return this.hubHandler;
    }

    protected void scheduleUpdateForDevice(int deviceId) {
        if (true == true) {
            return;
        }
        log.info("Scheduling an update request for deviceId=" + deviceId);
        scheduler.submit(new Runnable() {
            @Override
            public void run() {
                WinkHandler handler = getWinkHandler();
                if (handler != null) {
                    onDeviceStateChanged(handler.getLightDeviceById(deviceId));
                }
            }
        });
    }

    public WinkDevice getDevice() {
        if (device != null) {
            return device;
        }

        WinkHandler handler = getWinkHandler();
        device = handler.getLightDeviceById(deviceId);
        return device;
    }

    @Override
    public void bridgeStatusChanged(ThingStatusInfo bridgeStatus) {
        super.bridgeStatusChanged(bridgeStatus);
        if (bridgeStatus.getStatus() == ThingStatus.ONLINE) {
            scheduleUpdateForDevice(deviceId);
        }
    }

    @Override
    public void channelLinked(ChannelUID channelUID) {
        if (this.getBridge().getStatus() == ThingStatus.ONLINE) {
            // TODO really only need 1 for each device, no matter the channelse.
            scheduleUpdateForDevice(deviceId);
        } else {
            log.info("Channel Linked but hub is not online.");
        }
    }

    @Override
    public void onDeviceFound(WinkDevice d) {
        if (d.getId() == deviceId) {
            updateStatus(ThingStatus.ONLINE);
            onDeviceStateChanged(d);
        }
    }

    @Override
    public void onDeviceRemoved(WinkDevice d) {
        if (d.getId() == deviceId) {
            updateStatus(ThingStatus.OFFLINE);
        }
    }

    @Override
    public void onDeviceStateChanged(WinkDevice d) {
        if (d.getId() != deviceId) {
            return;
        }

        if (d.hasDesiredState()) {
            log.info("Received notice of pending state change.");
            return;
        }

        if (false && device != null && d.getLastReading().equals(device.getLastReading())) {
            log.info("Wink Device: " + d.getName() + " Received State Changed but no difference");
            return;
        }

        device = d;

        log.info("Wink Device: " + d.getName() + " State Changed: " + d.getLastReading());

        // TODO we should keep the previous state so that we don't send unnecessary updates.

        PercentType percentType = LightStateConverter.toBrightnessPercentType(d);
        if (!d.getLastReading().getPowered()) {
            percentType = new PercentType(0);
        }
        log.info("WinkDevice: " + d.getName() + " Light Level: " + percentType.intValue());
        updateState(powerLevelChannelName, percentType);

        // OnOffType onOffType;
        //
        // if (d.getLastReading().getPowered()) {
        // onOffType = OnOffType.ON;
        // } else {
        // onOffType = OnOffType.OFF;
        // }
        // log.info("WinkDevice: " + d.getName() + " Light State: " + onOffType.name());
        // updateState(powerSwitchChannelName, onOffType);
    }

    protected Integer getCurrentLevel(WinkDevice light) {
        Integer brightness = (int) (light.getLastReading().getBrightness() * 100);
        if (brightness == null && light != null) {
            if (!light.getLastReading().getPowered()) {
                brightness = 0;
            } else {
                brightness = (int) (light.getLastReading().getBrightness() * 100);
            }
        }
        return brightness;
    }

    protected StateUpdate convertLevelChangeToStateUpdate(IncreaseDecreaseType command, WinkDevice light) {
        StateUpdate stateUpdate = null;
        Integer currentBrightness = getCurrentLevel(light);
        if (currentBrightness != null) {
            int newBrightness = LightStateConverter.toAdjustedBrightness(command, currentBrightness);
            stateUpdate = createLevelStateUpdate(currentBrightness, newBrightness);
            // lastSentBrightness = newBrightness;
        }
        return stateUpdate;
    }

    protected StateUpdate createLevelStateUpdate(int currentBrightness, int newBrightness) {
        StateUpdate lightUpdate = new StateUpdate();
        if (newBrightness == 0) {
            lightUpdate.turnOff();
        } else {
            lightUpdate.setBrightness(newBrightness);
            if (currentBrightness == 0) {
                lightUpdate.turnOn();
            }
        }
        return lightUpdate;
    }

}
