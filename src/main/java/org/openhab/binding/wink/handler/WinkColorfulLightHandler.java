/**
 * Copyright (c) 2014-2016 by the respective copyright holders.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.wink.handler;

import org.eclipse.smarthome.core.library.types.HSBType;
import org.eclipse.smarthome.core.library.types.IncreaseDecreaseType;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.library.types.PercentType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.types.Command;
import org.openhab.binding.wink.WinkBindingConstants;
import org.openhab.binding.wink.internal.StateUpdate;
import org.openhab.binding.wink.model.WinkDevice;

/**
 * The {@link WinkColorfulLightHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Welliver - Initial contribution
 */
public class WinkColorfulLightHandler extends WinkDimmableLightHandler {

    protected String colorModel;
    protected String color;
    protected float colorX;
    protected float colorY;

    public WinkColorfulLightHandler(Thing thing) {
        super(thing);
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        StateUpdate lightState = null;
        String ch = channelUID.getId();
        log.warn("Got a command for channel id=" + ch);
        switch (ch) {
            case WinkBindingConstants.CHANNEL_LIGHT_COLOR:

                if (command instanceof HSBType) {
                    HSBType hsbCommand = (HSBType) command;
                    if (hsbCommand.getBrightness().intValue() == 0) {
                        lightState = LightStateConverter.toOnOffLightState(OnOffType.OFF);
                    } else {
                        lightState = LightStateConverter.toColorLightState(hsbCommand);
                    }
                } else if (command instanceof PercentType) {
                    lightState = LightStateConverter.toBrightnessLightState((PercentType) command);
                } else if (command instanceof OnOffType) {
                    lightState = LightStateConverter.toOnOffLightState((OnOffType) command, getDevice());
                } else if (command instanceof IncreaseDecreaseType) {
                    lightState = convertLevelChangeToStateUpdate((IncreaseDecreaseType) command, getDevice());
                }
                break;

            case WinkBindingConstants.CHANNEL_LIGHT_COLORTEMPERATURE:
                if (command instanceof PercentType) {
                    lightState = LightStateConverter.toColorTemperatureLightState((PercentType) command);
                } else if (command instanceof OnOffType) {
                    lightState = LightStateConverter.toOnOffLightState((OnOffType) command, getDevice());
                } else if (command instanceof IncreaseDecreaseType) {
                    lightState = convertColorTempChangeToStateUpdate((IncreaseDecreaseType) command, getDevice());
                }
                break;
        }

        if (lightState != null) {
            updateDeviceState(lightState);
        } else {
            super.handleCommand(channelUID, command);
        }
    }

    private StateUpdate convertColorTempChangeToStateUpdate(IncreaseDecreaseType command, WinkDevice light) {
        StateUpdate stateUpdate = null;
        Integer currentColorTemp = getCurrentColorTemp(light);
        if (currentColorTemp != null) {
            int newColorTemp = LightStateConverter.toAdjustedColorTemp(command, currentColorTemp);
            stateUpdate = new StateUpdate().setColorTemperature(newColorTemp);
        }
        return stateUpdate;
    }

    private Integer getCurrentColorTemp(WinkDevice light) {
        Integer colorTemp = light.getLastReading().getColor_temperature();

        return colorTemp;
    }

    @Override
    public void onDeviceStateChanged(WinkDevice d) {
        if (d.getId() != deviceId) {
            return;
        }

        if (d.hasDesiredState()) {
            log.info("Received notice of pending state change.");
            return;
        }

        if (false && device != null && d.getLastReading().equals(device.getLastReading())) {
            log.info("Wink Device: " + d.getName() + " Received State Changed but no difference");
            return;
        }

        // save state for next check.
        device = d;

        log.info("Wink Device: " + d.getName() + " State Changed: " + d.getLastReading());

        HSBType hsbType = LightStateConverter.toHSBType(d);
        if (!d.getLastReading().getPowered()) {
            // hsbType = new HSBType(hsbType.getHue(), hsbType.getSaturation(), new PercentType(0));
        }
        log.info("WinkDevice: " + d.getName() + " Color: " + hsbType.toFullString());

        updateState(WinkBindingConstants.CHANNEL_LIGHT_COLOR, hsbType);

        PercentType percentType = LightStateConverter.toColorTemperaturePercentType(d);
        log.info("WinkDevice: " + d.getName() + " Color Temperature: " + percentType.intValue());
        updateState(WinkBindingConstants.CHANNEL_LIGHT_COLORTEMPERATURE, percentType);

        log.info("WinkDevice: " + d.getName() + " Level: " + hsbType.getBrightness().intValue());
        updateState(WinkBindingConstants.CHANNEL_LIGHT_LEVEL, hsbType.getBrightness());

        OnOffType onOffType;

        if (d.getLastReading().getBrightness() > 0.0) {
            onOffType = OnOffType.ON;
        } else {
            onOffType = OnOffType.OFF;
        }
        log.info("WinkDevice: " + d.getName() + " Light State: " + onOffType.name());
        updateState(WinkBindingConstants.CHANNEL_LIGHT_STATE, onOffType);
    }
}
