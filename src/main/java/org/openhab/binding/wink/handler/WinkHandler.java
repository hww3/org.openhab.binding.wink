/**
 * Copyright (c) 2014-2016 by the respective copyright holders.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.wink.handler;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.UriBuilder;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicLineParser;
import org.apache.http.util.EntityUtils;
import org.eclipse.smarthome.config.core.Configuration;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.thing.binding.BaseBridgeHandler;
import org.eclipse.smarthome.core.types.Command;
import org.openhab.binding.wink.WinkBindingConstants;
import org.openhab.binding.wink.internal.StateUpdate;
import org.openhab.binding.wink.model.WinkDevice;
import org.openhab.binding.wink.model.WinkDeviceList;
import org.openhab.binding.wink.model.WinkDeviceResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

/**
 * The {@link WinkHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Welliver - Initial contribution
 */
public class WinkHandler extends BaseBridgeHandler {

    private Logger log = LoggerFactory.getLogger(WinkHandler.class);

    private int DEVICE_SCAN_INTERVAL = 60; // every 5 minutes

    // private ObjectMapper objectMapper = new ObjectMapper();
    private ResponseHandler<String> handler = new BasicResponseHandler();
    private Future<?> initializeTask;
    private Future<?> updatesTask;
    private Collection<DeviceStatusListener> deviceStatusListeners = new HashSet<>();

    private CloseableHttpClient httpclient;
    private CloseableHttpClient postHttpclient;
    private CloseableHttpClient updatesHttpclient;

    // Create a trust manager that does not validate certificate chains
    TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
        @Override
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return null;
        }

        @Override
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }

        @Override
        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
    } };

    private String token;

    private boolean shouldRunUpdates;

    public WinkHandler(Bridge thing) {
        super(thing);

        this.httpclient = buildHttpClient();
        this.postHttpclient = buildHttpClient();
        this.updatesHttpclient = buildUpdatesHttpClient();
    }

    CloseableHttpClient buildUpdatesHttpClient() {
        /*
         * SSLContext sc;
         * try {
         * sc = SSLContext.getInstance("SSL");
         * sc.init(null, trustAllCerts, new java.security.SecureRandom());
         * } catch (NoSuchAlgorithmException | KeyManagementException e) {
         * log.error("Unable to configure SSL context: " + e.getMessage());
         * throw (new RuntimeException(e));
         * }
         *
         * SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(sc,
         * SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
         *
         * Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
         * .register("https", sslConnectionFactory).build();
         *
         * HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);
         * MinimalHttpClient c = new MinimalHttpClient(ccm);
         * return c;
         */
        SSLContext sc;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            log.error("Unable to configure SSL context: " + e.getMessage());
            throw (new RuntimeException(e));
        }

        HttpClientBuilder builder = HttpClientBuilder.create();
        SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(sc,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        builder.setSSLSocketFactory(sslConnectionFactory);
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
                .register("https", sslConnectionFactory).build();

        HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);

        builder.setConnectionManager(ccm);

        return builder.build();

    }

    CloseableHttpClient buildHttpClient() {
        SSLContext sc;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            log.error("Unable to configure SSL context: " + e.getMessage());
            throw (new RuntimeException(e));
        }

        HttpClientBuilder builder = HttpClientBuilder.create();
        SSLConnectionSocketFactory sslConnectionFactory = new SSLConnectionSocketFactory(sc,
                SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
        builder.setSSLSocketFactory(sslConnectionFactory);
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
                .register("https", sslConnectionFactory).build();

        HttpClientConnectionManager ccm = new BasicHttpClientConnectionManager(registry);

        builder.setConnectionManager(ccm);

        return builder.build();
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        // if (channelUID.getId().equals(CHANNEL_1)) {
        // TODO: handle command

        // Note: if communication with thing fails for some reason,
        // indicate that by setting the status with detail information
        // updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.COMMUNICATION_ERROR,
        // "Could not control device at IP address x.x.x.x");
        // }
    }

    public void updateUrl(String url) {
        Map<String, String> props = editProperties();
        props.put(WinkBindingConstants.PROPERTY_URL, url);
        updateProperties(props);
    }

    @Override
    public void initialize() {
        Configuration config = getThing().getConfiguration();
        Map<String, String> properties = getThing().getProperties();
        token = (String) config.get(WinkBindingConstants.CONFIG_TOKEN);

        final String uuid = properties.get(WinkBindingConstants.PROPERTY_UUID);
        final String u = properties.get(WinkBindingConstants.PROPERTY_URL);
        final URL url;
        try {
            url = new URL(u);
        } catch (MalformedURLException e) {
            goOffline(ThingStatusDetail.CONFIGURATION_ERROR, "Invalid device URL: " + e.getMessage());
            return;
        }

        scheduleCheckCommunication(1);
    }

    protected void checkCommunication() {
        log.debug("checking configuration");
        WinkResponse response = null;

        // Seems we can't do this now.
        // updateStatus(ThingStatus.INITIALIZING);

        Configuration config = getThing().getConfiguration();
        Map<String, String> properties = getThing().getProperties();
        token = (String) config.get(WinkBindingConstants.CONFIG_TOKEN);

        final String u = properties.get(WinkBindingConstants.PROPERTY_URL);

        final URL url;
        try {
            url = new URL(u);
        } catch (MalformedURLException e) {
            // attempting to retry will do no good. We must wait to rediscover a valid url.
            goOffline(ThingStatusDetail.CONFIGURATION_ERROR, "Invalid device URL: " + e.getMessage());
            return;
        }

        log.debug("Attempting to communicate with " + url);

        try {
            response = getConnection("/devices");
            log.debug("got response from wink: " + response.getStatusLine());
        } catch (Throwable e) {
            log.debug("communication error: " + e.getMessage());
            goOffline(ThingStatusDetail.COMMUNICATION_ERROR,
                    "Failed to connect to URL (" + u.toString() + "): " + e.getMessage());
            scheduleCheckCommunication(60);
            return;
        }

        // wink will return a 401 or 403 depending on whether the token is unknown or expired.
        // we should report accordingly.
        if (response.getCode() == 401) {
            log.debug("access denied");
            goOffline(ThingStatusDetail.CONFIGURATION_ERROR, "Invalid token");
            scheduleCheckCommunication(60);
            return;
        } else if (response.getCode() != 200) {
            log.debug("unknown error");
            goOffline(ThingStatusDetail.COMMUNICATION_ERROR, "Unexpected response: " + response.getCode());
            scheduleCheckCommunication(60);
            return;
        }

        log.debug("setting online");
        goOnline();
        return;
    }

    protected void scheduleCheckCommunication(int seconds) {
        // only one initialization task at a time, please.
        if (initializeTask != null && !initializeTask.isDone()) {
            initializeTask.cancel(true);
        }
        log.debug("running communication check in " + seconds + " seconds");
        initializeTask = scheduler.schedule(new Runnable() {
            @Override
            public void run() {
                checkCommunication();
            }
        }, seconds, TimeUnit.SECONDS);
    }

    protected void goOnline() {
        // we don't need to check communications if we're online already.
        // only one initialization task at a time, please.
        if (initializeTask != null && !initializeTask.isDone()) {
            initializeTask.cancel(true);
        }

        if (updatesTask != null && !updatesTask.isDone()) {
            updatesTask.cancel(true);
        }
        shouldRunUpdates = true;
        updateStatus(ThingStatus.ONLINE);
        checkDevices(5);
        startUpdatesTask();
    }

    protected void goOffline(ThingStatusDetail detail, String reason) {
        if (updatesTask != null && !updatesTask.isDone()) {
            updatesTask.cancel(true);
        }

        shouldRunUpdates = false;
        updateStatus(ThingStatus.OFFLINE, detail, reason);
        scheduleCheckCommunication(5);
    }

    void startUpdatesTask() {
        updatesTask = scheduler.submit(new Runnable() {
            @Override
            public void run() {
                if (shouldRunUpdates) {

                    // the updates check is a long running HTTP request that streams data from the HUB.
                    // if it should return (because the connection was closed, etc) we should attempt to
                    // reconnect promptly.
                    runUpdatesCheck();
                    startUpdatesTask();
                }
            }
        });
    }

    void runUpdatesCheck() {
        log.info("Starting update checker.");
        UriBuilder builder = UriBuilder.fromUri(getThing().getProperties().get(WinkBindingConstants.PROPERTY_URL));
        builder.path("/updates");

        HttpGet httpget = new HttpGet(builder.build());
        httpget.setHeader("Authorization",
                "Bearer " + getThing().getConfiguration().get(WinkBindingConstants.CONFIG_TOKEN));
        httpget.setConfig(requestConfigWithTimeout(10000));
        HttpResponse response = null;
        try {
            response = updatesHttpclient.execute(httpget);
        } catch (IOException e) {
            log.warn("Got error trying to read updates stream.", e);
            return;
        }

        HttpEntity entity = response.getEntity();

        BufferedInputStream bufferedInputStream;
        BufferedReader br = null;
        try {
            bufferedInputStream = new BufferedInputStream(entity.getContent());
            InputStreamReader reader = new InputStreamReader(bufferedInputStream);
            br = new BufferedReader(reader);
            int r;

            Map<String, String> message = new HashMap<>();
            boolean keepalive = false;
            do {
                r = 2;
                String s = br.readLine();
                if (s == null || s.isEmpty()) {
                    if (!message.isEmpty()) {
                        dispatchEvent(message);
                    }
                    keepalive = false;
                    message = new HashMap<>();
                } else {
                    if (keepalive) {
                        continue;
                    }
                    Header h = BasicLineParser.parseHeader(s, BasicLineParser.DEFAULT);
                    // log.info("got message: " + h.getName() + ", value=" + h.getValue());
                    if ("event".equals(h.getName()) && "keep-alive".equals(h.getValue())) {
                        keepalive = true;
                    } else {
                        if (log.isTraceEnabled()) {
                            log.trace("got async message: " + h.getName() + ", value=" + h.getValue());
                        }
                        message.put(h.getName(), h.getValue());
                    }
                }
            } while (r != -1);
        } catch (org.apache.http.conn.HttpHostConnectException hce) {
            log.warn("Got error trying to read updates stream.", hce);
            goOffline(ThingStatusDetail.COMMUNICATION_ERROR, "Failed to connect to hub: " + hce.getMessage());
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e1) {
                    // if closing fails, not much we can do.
                }
            }
            return;
        } catch (IllegalStateException | IOException e) {
            log.warn("Got error trying to read updates stream.", e);
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e1) {
                    // if closing fails, not much we can do.
                }
            }
            return;
        }

    }

    private void dispatchEvent(Map<String, String> pairs) {
        WinkDeviceResult device = null;
        String type = null;
        String d = pairs.get("data");
        type = pairs.get("event");
        // log.info("dispatchEvent got event: " + d);
        device = new Gson().fromJson(d, WinkDeviceResult.class);
        if ("PUT".equals(type) && device != null) {
            log.info("Got event from Wink Hub for device " + device.getData().getName() + ": " + d);
            informDeviceListeners(device.getData());
        } else if ("POST".equals(type) && device != null) {
            log.info("Got event from Wink Hub for device " + device.getData().getName() + ": " + d);
            triggerDeviceFound(device.getData());
        } else if ("DELETE".equals(type) && device != null) {
            log.info("Got event from Wink Hub for device " + device.getData().getName() + ": " + d);
            triggerDeviceRemoved(device.getData());
        } else if (device == null) {
            log.warn("got null device update of type " + type);
        } else {
            log.warn("got unknown update type " + type);
        }
    }

    void checkDevices(int seconds) {
        log.info("Should we schedule status check of Wink Devices?");
        if (shouldRunUpdates) {
            log.info("Yes, looks good, scheduling status check of Wink Devices");

            scheduler.schedule(new Runnable() {
                @Override
                public void run() {
                    log.info("Checking status of Wink Devices");
                    Collection<WinkDevice> devices = getDevices();
                    log.info("Wink reports on " + devices.size() + " devices.");
                    for (WinkDevice device : devices) {
                        informDeviceListeners(device);
                        triggerDeviceFound(device);
                    }
                    checkDevices(DEVICE_SCAN_INTERVAL);
                }
            }, seconds, TimeUnit.SECONDS);
        }
    }

    protected void informDeviceListeners(WinkDevice device) {
        for (DeviceStatusListener s : deviceStatusListeners) {
            s.onDeviceStateChanged(device);
        }
    }

    protected void triggerDeviceFound(WinkDevice device) {
        for (DeviceStatusListener s : deviceStatusListeners) {
            s.onDeviceFound(device);
        }
    }

    protected void triggerDeviceRemoved(WinkDevice device) {
        for (DeviceStatusListener s : deviceStatusListeners) {
            s.onDeviceRemoved(device);
        }
    }

    public WinkDevice getLightDeviceById(int deviceId) {
        WinkResponse response;
        String responseData;

        try {
            response = getConnection("/light/" + deviceId);

        } catch (IOException e) {
            log.error("Error occurred while getting device list.", e);
            return null;
        }

        // wink will return a 401 or 403 depending on whether the token is unknown or expired.
        // we should report accordingly.

        if (response.getCode() == 401) {
            goOffline(ThingStatusDetail.CONFIGURATION_ERROR, "Invalid credentials");
            return null;
        } else if (response.getCode() == 403) {
            goOffline(ThingStatusDetail.CONFIGURATION_ERROR, "Invalid credentials");
            return null;
        } else if (response.getCode() != 200) {
            goOffline(ThingStatusDetail.COMMUNICATION_ERROR, "Unknown error");
            return null;
        }
        // we need to look at status and pagination at some point, too
        WinkDeviceResult result = new Gson().fromJson(response.getData(), WinkDeviceResult.class);
        return result.getData();
    }

    public Collection<WinkDevice> getDevices() {
        Collection<WinkDevice> devices = new HashSet<>();
        WinkResponse response;
        String responseData;

        try {
            response = getConnection("/devices");
        } catch (IOException e) {
            log.error("Error occurred while getting device list.", e);
            return devices;
        }

        if (response.getCode() == 403) {
            goOffline(ThingStatusDetail.CONFIGURATION_ERROR, "Invalid credentials");
            return Collections.emptySet();
        } else if (response.getCode() == 401) {
            goOffline(ThingStatusDetail.CONFIGURATION_ERROR, "Invalid credentials");
            return Collections.emptySet();
        } else if (response.getCode() != 200) {
            goOffline(ThingStatusDetail.COMMUNICATION_ERROR, "Unknown error");
            return Collections.emptySet();
        }

        // we need to look at status and pagination at some point, too
        WinkDeviceList list = new Gson().fromJson(response.getData(), WinkDeviceList.class);
        return list.getData();
    }

    public void setLightDimmerById(int deviceId, boolean powered, double brightness) {
        Map<String, Object> c = new HashMap<>();
        c.put("brightness", brightness);
        c.put("powered", powered);
        setLightDesiredState(deviceId, c);
    }

    protected void setLightDesiredState(int deviceId, StateUpdate desiredState) {
        WinkResponse response;

        try {
            Map<String, Object> m = new HashMap<>();

            String json = "{\"desired_state\": " + desiredState.toJson() + "}";

            response = postConnection("/lights/" + deviceId, json);
        } catch (IOException e) {
            log.error("Error occurred while getting device list.", e);
            return;
        }

        // TODO response checking
        // updateLightStateById(deviceId);
    }

    protected void setLightDesiredState(int deviceId, Map<String, Object> desiredState) {
        WinkResponse response;

        try {
            Map<String, Object> m = new HashMap<>();
            m.put("desired_state", desiredState);
            response = postConnection("/lights/" + deviceId, m);
        } catch (IOException e) {
            log.error("Error occurred while getting device list.", e);
            return;
        }

        // TODO response checking
        // updateLightStateById(deviceId);
    }

    protected void updateLightStateById(int deviceId) {
        WinkDevice device = getLightDeviceById(deviceId);
        if (device != null) {
            informDeviceListeners(device);
        }
    }

    protected WinkResponse getConnection() throws IOException {
        return getConnection(null);
    }

    protected synchronized WinkResponse getConnection(String path) throws IOException {

        UriBuilder builder = UriBuilder.fromUri(getThing().getProperties().get(WinkBindingConstants.PROPERTY_URL));
        if (path != null) {
            builder.path(path);
        }

        HttpGet httpget = new HttpGet(builder.build());
        httpget.setConfig(requestConfigWithTimeout(10000));
        httpget.setHeader("Authorization",
                "Bearer " + getThing().getConfiguration().get(WinkBindingConstants.CONFIG_TOKEN));

        HttpResponse response = httpclient.execute(httpget);
        HttpEntity entity = response.getEntity();
        String responseData = EntityUtils.toString(entity, "UTF-8");
        WinkResponse winkResponse = new WinkResponse(response, responseData);

        return winkResponse;
    }

    protected synchronized WinkResponse postConnection(String path, Map<String, Object> params) throws IOException {
        return postConnection(path, new Gson().toJson(params));
    }

    protected synchronized WinkResponse postConnection(String path, String body) throws IOException {

        UriBuilder builder = UriBuilder.fromUri(getThing().getProperties().get(WinkBindingConstants.PROPERTY_URL));
        if (path != null) {
            builder.path(path);
        }

        HttpPut post = new HttpPut(builder.build());
        post.setConfig(requestConfigWithTimeout(10000));

        log.info("Sending command: " + path + " : " + body);
        StringEntity postingString = new StringEntity(body);// gson.tojson() converts
                                                            // your pojo to json
        post.setEntity(postingString);
        post.setHeader("Content-type", "application/json");
        post.setHeader("Authorization",
                "Bearer " + getThing().getConfiguration().get(WinkBindingConstants.CONFIG_TOKEN));
        HttpResponse response = postHttpclient.execute(post);

        HttpEntity entity = response.getEntity();
        String responseData = EntityUtils.toString(entity, "UTF-8");
        WinkResponse winkResponse = new WinkResponse(response, responseData);

        return winkResponse;
    }

    protected RequestConfig requestConfigWithTimeout(int timeoutInMilliseconds) {
        return RequestConfig.copy(RequestConfig.DEFAULT).setSocketTimeout(timeoutInMilliseconds)
                .setConnectTimeout(timeoutInMilliseconds).setConnectionRequestTimeout(timeoutInMilliseconds).build();
    }

    public void registerDeviceStatusListener(DeviceStatusListener winkDeviceDiscoveryService) {
        deviceStatusListeners.add(winkDeviceDiscoveryService);
    }

    public void unregisterDeviceStatusListener(DeviceStatusListener winkDeviceDiscoveryService) {
        deviceStatusListeners.remove(winkDeviceDiscoveryService);
    }

}
