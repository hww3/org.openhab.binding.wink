package org.openhab.binding.wink.handler;

import org.apache.http.HttpResponse;

public class WinkResponse {
    private int code;
    private String data;
    private String statusLine;

    public String getStatusLine() {
        return statusLine;
    }

    public int getCode() {
        return code;
    }

    public String getData() {
        return data;
    }

    public WinkResponse(HttpResponse response, String data) {
        this.code = response.getStatusLine().getStatusCode();
        this.statusLine = response.getStatusLine().toString();
        this.data = data;
    }
}
