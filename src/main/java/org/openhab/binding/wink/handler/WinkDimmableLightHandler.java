/**
 * Copyright (c) 2014-2016 by the respective copyright holders.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.wink.handler;

import org.eclipse.smarthome.core.thing.Thing;
import org.openhab.binding.wink.WinkBindingConstants;

/**
 * The {@link WinkDimmableLightHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Welliver - Initial contribution
 */
public class WinkDimmableLightHandler extends WinkPowerLevelDeviceHandler {

    public WinkDimmableLightHandler(Thing thing) {
        super(thing, WinkBindingConstants.CHANNEL_LIGHT_LEVEL, WinkBindingConstants.CHANNEL_LIGHT_STATE);
    }

}
