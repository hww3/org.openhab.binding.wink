/**
 * Copyright (c) 2014-2016 by the respective copyright holders.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.wink;

import java.util.Set;

import org.eclipse.smarthome.core.thing.ThingTypeUID;

import com.google.common.collect.ImmutableSet;

/**
 * The {@link WinkBinding} class defines common constants, which are
 * used across the whole binding.
 *
 * @author William Welliver - Initial contribution
 */
public class WinkBindingConstants {

    public static final String BINDING_ID = "wink";

    // List of all Thing Type UIDs
    public final static ThingTypeUID THING_TYPE_WINKHUB = new ThingTypeUID(BINDING_ID, "hub");
    public final static ThingTypeUID THING_TYPE_WINKHUB2 = new ThingTypeUID(BINDING_ID, "hub2");

    public final static ThingTypeUID THING_TYPE_DIMMABLE_LIGHT = new ThingTypeUID(BINDING_ID, "dimmableLight");
    public final static ThingTypeUID THING_TYPE_COLORFUL_LIGHT = new ThingTypeUID(BINDING_ID, "colorfulLight");
    public final static ThingTypeUID THING_TYPE_VARIABLE_FAN = new ThingTypeUID(BINDING_ID, "variableFan");

    public final static Set<String> SUPPORTED_DEVICE_MODELS = ImmutableSet.of("Hub");

    public final static Set<ThingTypeUID> SUPPORTED_HUBS_UIDS = ImmutableSet.of(THING_TYPE_WINKHUB,
            THING_TYPE_WINKHUB2);

    public final static Set<ThingTypeUID> SUPPORTED_THING_TYPES_UIDS = ImmutableSet.of(THING_TYPE_DIMMABLE_LIGHT,
            THING_TYPE_COLORFUL_LIGHT, THING_TYPE_VARIABLE_FAN);

    public final static String PROPERTY_UUID = "uuid";
    public final static String PROPERTY_URL = "url";

    public final static String PROPERTY_OBJECT_ID = "objectId";
    public final static String PROPERTY_OBJECT_NAME = "name";

    public final static String CONFIG_TOKEN = "token";

    // List of all Channel ids
    public final static String CHANNEL_LIGHT_LEVEL = "lightlevel";
    public final static String CHANNEL_LIGHT_STATE = "lightstate";

    public final static String CHANNEL_LIGHT_COLORTEMPERATURE = "colorTemperature";
    public final static String CHANNEL_LIGHT_COLOR = "color";

    public final static String CHANNEL_FAN_SPEED = "fanSpeed";
    public final static String CHANNEL_POWER_SWITCH = "powerSwitch";
}
