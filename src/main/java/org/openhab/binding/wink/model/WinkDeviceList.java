package org.openhab.binding.wink.model;

import java.util.List;

public class WinkDeviceList {
    protected List<WinkDevice> data;

    public List<WinkDevice> getData() {
        return data;
    }

    public void setData(List<WinkDevice> data) {
        this.data = data;
    }
}