package org.openhab.binding.wink.model;

import java.util.Map;

public class WinkDevice {

    int local_id;
    String name;
    double updated_at;
    double created_at;
    double discovered_at;
    Map<String, Object> desired_state;

    WinkDeviceState last_reading;

    public int getId() {
        return local_id;
    }

    public String getName() {
        return name;
    }

    public boolean hasDesiredState() {
        if (desired_state != null && !desired_state.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public WinkDeviceState getLastReading() {
        return last_reading;
    }
}