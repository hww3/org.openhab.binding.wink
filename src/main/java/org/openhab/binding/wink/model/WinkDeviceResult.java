package org.openhab.binding.wink.model;

public class WinkDeviceResult {
    protected WinkDevice data;

    public WinkDevice getData() {
        return data;
    }

    public void setData(WinkDevice data) {
        this.data = data;
    }

}