package org.openhab.binding.wink.model;

public class WinkDeviceState {
    protected Boolean connection;
    protected Boolean powered;
    protected float brightness;
    protected String color_model; // "rgb", "xy", "hsb", "color_temperature"
    protected float color_x;
    protected float color_y;
    protected float hue;
    protected float saturation;
    protected int color_temperature;
    protected String color;
    protected String powering_mode;

    public Boolean getConnection() {
        return connection;
    }

    public void setConnection(Boolean connection) {
        this.connection = connection;
    }

    public Boolean getPowered() {
        return powered;
    }

    public void setPowered(Boolean powered) {
        this.powered = powered;
    }

    public float getBrightness() {
        return brightness;
    }

    public void setBrightness(float brightness) {
        this.brightness = brightness;
    }

    public String getColor_model() {
        return color_model;
    }

    public void setColor_model(String color_model) {
        this.color_model = color_model;
    }

    public float getColor_x() {
        return color_x;
    }

    public void setColor_x(float color_x) {
        this.color_x = color_x;
    }

    public float getColor_y() {
        return color_y;
    }

    public void setColor_y(float color_y) {
        this.color_y = color_y;
    }

    public float getHue() {
        return hue;
    }

    public void setHue(float hue) {
        this.hue = hue;
    }

    public float getSaturation() {
        return saturation;
    }

    public void setSaturation(float saturation) {
        this.saturation = saturation;
    }

    public int getColor_temperature() {
        return color_temperature;
    }

    public void setColor_temperature(int color_temperature) {
        this.color_temperature = color_temperature;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPowering_mode() {
        return powering_mode;
    }

    public void setPowering_mode(String powering_mode) {
        this.powering_mode = powering_mode;
    }

    @Override
    public String toString() {
        return "WinkDeviceState [connection=" + connection + ", powered=" + powered + ", brightness=" + brightness
                + ", color_model=" + color_model + ", color_x=" + color_x + ", color_y=" + color_y + ", hue=" + hue
                + ", saturation=" + saturation + ", color_temperature=" + color_temperature + ", color=" + color + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(brightness);
        result = prime * result + ((color == null) ? 0 : color.hashCode());
        result = prime * result + ((color_model == null) ? 0 : color_model.hashCode());
        result = prime * result + color_temperature;
        result = prime * result + Float.floatToIntBits(color_x);
        result = prime * result + Float.floatToIntBits(color_y);
        result = prime * result + ((connection == null) ? 0 : connection.hashCode());
        result = prime * result + Float.floatToIntBits(hue);
        result = prime * result + ((powered == null) ? 0 : powered.hashCode());
        result = prime * result + ((powering_mode == null) ? 0 : powering_mode.hashCode());
        result = prime * result + Float.floatToIntBits(saturation);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        WinkDeviceState other = (WinkDeviceState) obj;
        if (Float.floatToIntBits(brightness) != Float.floatToIntBits(other.brightness)) {
            return false;
        }
        if (color == null) {
            if (other.color != null) {
                return false;
            }
        } else if (!color.equals(other.color)) {
            return false;
        }
        if (color_model == null) {
            if (other.color_model != null) {
                return false;
            }
        } else if (!color_model.equals(other.color_model)) {
            return false;
        }
        if (color_temperature != other.color_temperature) {
            return false;
        }
        if (Float.floatToIntBits(color_x) != Float.floatToIntBits(other.color_x)) {
            return false;
        }
        if (Float.floatToIntBits(color_y) != Float.floatToIntBits(other.color_y)) {
            return false;
        }
        if (connection == null) {
            if (other.connection != null) {
                return false;
            }
        } else if (!connection.equals(other.connection)) {
            return false;
        }
        if (Float.floatToIntBits(hue) != Float.floatToIntBits(other.hue)) {
            return false;
        }
        if (powered == null) {
            if (other.powered != null) {
                return false;
            }
        } else if (!powered.equals(other.powered)) {
            return false;
        }
        if (powering_mode == null) {
            if (other.powering_mode != null) {
                return false;
            }
        } else if (!powering_mode.equals(other.powering_mode)) {
            return false;
        }
        if (Float.floatToIntBits(saturation) != Float.floatToIntBits(other.saturation)) {
            return false;
        }
        return true;
    }
}