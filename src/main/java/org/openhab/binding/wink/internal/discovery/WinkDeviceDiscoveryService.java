package org.openhab.binding.wink.internal.discovery;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.smarthome.config.discovery.AbstractDiscoveryService;
import org.eclipse.smarthome.config.discovery.DiscoveryResult;
import org.eclipse.smarthome.config.discovery.DiscoveryResultBuilder;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.openhab.binding.wink.WinkBindingConstants;
import org.openhab.binding.wink.handler.DeviceStatusListener;
import org.openhab.binding.wink.handler.WinkHandler;
import org.openhab.binding.wink.internal.WinkHandlerFactory;
import org.openhab.binding.wink.model.WinkDevice;
import org.openhab.binding.wink.model.WinkDeviceState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WinkDeviceDiscoveryService extends AbstractDiscoveryService implements DeviceStatusListener {
    private final static int SEARCH_TIME = 60;
    private final Logger logger = LoggerFactory.getLogger(WinkDeviceDiscoveryService.class);

    private WinkHandler winkHandler;

    public WinkDeviceDiscoveryService(WinkHandler winkHandler) {
        super(SEARCH_TIME);
        this.winkHandler = winkHandler;
    }

    public void activate() {
        logger.debug("activate");
        winkHandler.registerDeviceStatusListener(this);
        // startScan();
    }

    @Override
    public void deactivate() {
        removeOlderResults(new Date().getTime());
        winkHandler.unregisterDeviceStatusListener(this);
    }

    @Override
    public Set<ThingTypeUID> getSupportedThingTypes() {
        return WinkHandlerFactory.SUPPORTED_THING_TYPES_UIDS;
    }

    @Override
    public void startScan() {

        Collection<WinkDevice> devices = winkHandler.getDevices();

        for (WinkDevice d : devices) {
            onDeviceFound(d);
        }

    }

    @Override
    public void onDeviceFound(WinkDevice d) {
        ThingTypeUID thingTypeUID = getThingTypeUID(d);
        if (thingTypeUID == null) {
            return;
        }

        ThingUID thingUID = getThingUID(d);

        if (thingUID != null) {
            if (winkHandler.getThingByUID(thingUID) != null) {
                logger.debug("ignoring already registered device of name '{}' with id {}", d.getName(), d.getId());
            }
            ThingUID bridgeUID = winkHandler.getThing().getUID();
            Map<String, Object> properties = new HashMap<>(1);
            properties.put(WinkBindingConstants.PROPERTY_OBJECT_ID, d.getId());
            properties.put(WinkBindingConstants.PROPERTY_OBJECT_NAME, d.getName());

            DiscoveryResult discoveryResult = DiscoveryResultBuilder.create(thingUID).withThingType(thingTypeUID)
                    .withProperties(properties).withBridge(bridgeUID).withLabel(d.getName()).build();

            thingDiscovered(discoveryResult);
        } else {
            logger.debug("discovered unsupported device of name '{}' with id {}", d.getName(), d.getId());
        }

    }

    @Override
    protected synchronized void stopScan() {
        super.stopScan();
        removeOlderResults(getTimestampOfLastScan());
    }

    private ThingUID getThingUID(WinkDevice device) {
        ThingUID bridgeUID = winkHandler.getThing().getUID();
        ThingTypeUID thingTypeUID = getThingTypeUID(device);

        if (thingTypeUID != null && getSupportedThingTypes().contains(thingTypeUID)) {
            return new ThingUID(thingTypeUID, bridgeUID, "" + device.getId());
        } else {
            return null;
        }
    }

    private ThingTypeUID getThingTypeUID(WinkDevice device) {
        ThingTypeUID thingTypeId = null;
        String name = device.getName();
        WinkDeviceState state = device.getLastReading();
        if (name == null || name.contains("Pico") || name.contains("Remote")) {
            return null; // no pico remotes yet
        }
        if (state.getColor_model() != null) {
            thingTypeId = WinkBindingConstants.THING_TYPE_COLORFUL_LIGHT;
        } else if (state.getPowered() == null) {
            thingTypeId = null; // probably the hub object
        } else {
            if (name.contains("Fan")) {
                thingTypeId = WinkBindingConstants.THING_TYPE_VARIABLE_FAN;
            } else {
                thingTypeId = WinkBindingConstants.THING_TYPE_DIMMABLE_LIGHT;
            }
        }
        return thingTypeId;
    }

    @Override
    public void onDeviceRemoved(WinkDevice d) {
        // TODO Remove devices

    }

    @Override
    public void onDeviceStateChanged(WinkDevice light) {
        // Do nothing
    }
}
