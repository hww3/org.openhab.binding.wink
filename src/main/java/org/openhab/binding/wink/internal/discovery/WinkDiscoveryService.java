package org.openhab.binding.wink.internal.discovery;

import java.util.Set;

import org.apache.http.HttpResponse;
import org.eclipse.smarthome.config.discovery.DiscoveryResult;
import org.eclipse.smarthome.config.discovery.DiscoveryResultBuilder;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.openhab.binding.wink.WinkBindingConstants;
import org.openhab.binding.wink.handler.WinkHandler;

public class WinkDiscoveryService extends AbstractSSDPDiscoveryService {

    private static final String WINK_DISCOVERY_MESSAGE = "M-SEARCH * HTTP/1.1\r\n" + "Host: 239.255.255.250:1900\r\n"
            + "MAN: \"ssdp:discover\"\r\n" + "MX: 12\r\n" + "ST: urn:wink-com:service:fasterLights:2\r\n" + "\r\n";

    /**
     * Copyright (c) 2014-2016 by the respective copyright holders.
     * All rights reserved. This program and the accompanying materials
     * are made available under the terms of the Eclipse Public License v1.0
     * which accompanies this distribution, and is available at
     * http://www.eclipse.org/legal/epl-v10.html
     */

    protected final static Set<ThingTypeUID> supportedDevices = WinkBindingConstants.SUPPORTED_HUBS_UIDS;

    public WinkDiscoveryService() {
        super(supportedDevices, 20);
    }

    @Override
    protected String getScanWords() {
        return "fasterLights:2";
    }

    @Override
    protected String getDiscoveryMessage() {
        return WinkDiscoveryService.WINK_DISCOVERY_MESSAGE;
    }

    @Override
    protected void parseResponse(HttpResponse response) {
        DiscoveryResult result;
        String label = "";
        String st = "";
        String url = "";
        String uuid = "";

        // log.debug("extracting data.");
        org.apache.http.Header h = response.getFirstHeader("ST");
        if (h != null) {
            st = h.getValue();
        }

        if (!st.equalsIgnoreCase("urn:wink-com:service:fasterLights:2")) {
            return;
        }

        h = response.getFirstHeader("Location");
        if (h != null) {
            url = h.getValue();
        }

        h = response.getFirstHeader("USN");
        if (h != null) {
            uuid = h.getValue();
            int s, f;

            s = uuid.indexOf("uuid:");
            if (s < 0) {
                return;
            }

            f = uuid.indexOf("::");

            uuid = uuid.substring(s + 5, f);

            label = "Wink Hub";
        }

        ThingUID thingUid = new ThingUID(WinkBindingConstants.THING_TYPE_WINKHUB, uuid.replace(":", ""));

        // discoveredDevices.put(uuid, System.currentTimeMillis());
        log.debug("Got discovered device.");
        if (discoveryServiceCallback != null) {
            log.debug("Looking to see if this thing exists already.");
            Thing thing = discoveryServiceCallback.getExistingThing(thingUid);
            if (thing != null) {
                this.backgroundScanInterval = 300;
                log.debug("Already have wink hub with ID=<" + thingUid + ">");
                String thingUrl = thing.getProperties().get(WinkBindingConstants.PROPERTY_URL);
                log.debug("ThingURL=<" + thingUrl + ">, discoveredUrl=<" + url + ">");
                this.backgroundScanInterval = 300;
                if (thingUrl == null || !thingUrl.equals(url)) {
                    ((WinkHandler) thing.getHandler()).updateUrl(url);
                    ((WinkHandler) thing.getHandler()).thingUpdated(thing);
                    log.info("Updated url for existing Hub => " + url);
                }
                return;
            } else {
                log.debug("Nope. This should trigger a new inbox entry.");
            }
        } else {
            log.warn("DiscoveryServiceCallback not set. This shouldn't happen!");
        }

        result = DiscoveryResultBuilder.create(thingUid).withLabel(label).withRepresentationProperty(uuid)
                .withProperty(WinkBindingConstants.PROPERTY_UUID, uuid)
                .withProperty(WinkBindingConstants.PROPERTY_URL, url).build();
        log.info("New wink hub discovered with ID=<" + uuid.replace(":", "") + ">.");
        this.thingDiscovered(result);
    }
}
