/**
 * Copyright (c) 2014-2016 by the respective copyright holders.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.wink.internal;

import static org.openhab.binding.wink.WinkBindingConstants.*;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;

import org.eclipse.smarthome.config.discovery.DiscoveryService;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.ThingUID;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandlerFactory;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.openhab.binding.wink.WinkBindingConstants;
import org.openhab.binding.wink.handler.WinkColorfulLightHandler;
import org.openhab.binding.wink.handler.WinkDimmableLightHandler;
import org.openhab.binding.wink.handler.WinkHandler;
import org.openhab.binding.wink.handler.WinkVariableFanHandler;
import org.openhab.binding.wink.internal.discovery.WinkDeviceDiscoveryService;
import org.osgi.framework.ServiceRegistration;

/**
 * The {@link WinkHandlerFactory} is responsible for creating things and thing
 * handlers.
 *
 * @author William Welliver - Initial contribution
 */
public class WinkHandlerFactory extends BaseThingHandlerFactory {

    public final static Set<ThingTypeUID> SUPPORTED_THING_TYPES_UIDS = WinkBindingConstants.SUPPORTED_THING_TYPES_UIDS;
    private Map<ThingUID, ServiceRegistration<?>> discoveryServiceRegs = new HashMap<>();

    @Override
    public boolean supportsThingType(ThingTypeUID thingTypeUID) {
        return THING_TYPE_WINKHUB.equals(thingTypeUID) || SUPPORTED_THING_TYPES_UIDS.contains(thingTypeUID);
    }

    @Override
    protected ThingHandler createHandler(Thing thing) {

        ThingTypeUID thingTypeUID = thing.getThingTypeUID();

        if (thingTypeUID.equals(THING_TYPE_WINKHUB)) {
            WinkHandler handler = new WinkHandler((Bridge) thing);
            registerDeviceDiscoveryService(handler);
            return handler;
        } else if (thingTypeUID.equals(THING_TYPE_COLORFUL_LIGHT)) {
            WinkColorfulLightHandler handler = new WinkColorfulLightHandler(thing);
            return handler;

        } else if (thingTypeUID.equals(THING_TYPE_DIMMABLE_LIGHT)) {
            WinkDimmableLightHandler handler = new WinkDimmableLightHandler(thing);
            return handler;
        } else if (thingTypeUID.equals(THING_TYPE_VARIABLE_FAN)) {
            WinkVariableFanHandler handler = new WinkVariableFanHandler(thing);
            return handler;
        }

        return null;
    }

    private synchronized void registerDeviceDiscoveryService(WinkHandler hubHandler) {
        WinkDeviceDiscoveryService discoveryService = new WinkDeviceDiscoveryService(hubHandler);
        discoveryService.activate();
        this.discoveryServiceRegs.put(hubHandler.getThing().getUID(), bundleContext
                .registerService(DiscoveryService.class.getName(), discoveryService, new Hashtable<String, Object>()));
    }

    @Override
    protected synchronized void removeHandler(ThingHandler thingHandler) {
        if (thingHandler instanceof WinkHandler) {
            ServiceRegistration<?> serviceReg = this.discoveryServiceRegs.get(thingHandler.getThing().getUID());
            if (serviceReg != null) {
                // remove discovery service, if bridge handler is removed
                WinkDeviceDiscoveryService service = (WinkDeviceDiscoveryService) bundleContext
                        .getService(serviceReg.getReference());
                service.deactivate();
                serviceReg.unregister();
                discoveryServiceRegs.remove(thingHandler.getThing().getUID());
            }
        }
    }
}
