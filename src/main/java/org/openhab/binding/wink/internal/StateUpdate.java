/**
 * Copyright (c) 2014-2017 by the respective copyright holders.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.wink.internal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Collection of updates to the state of a light.
 *
 * @author Q42, standalone Jue library (https://github.com/Q42/Jue)
 * @author Thomas Höfer - added unique id and changed range check for brightness and saturation
 * @author Denis Dudnik - moved Jue library source code inside the smarthome Hue binding, minor code cleanup
 */
public class StateUpdate {
    ArrayList<Command> commands = new ArrayList<>();
    Set<String> commandsAdded = new HashSet<>();

    public String toJson() {
        StringBuilder json = new StringBuilder("{");

        for (int i = 0; i < commands.size(); i++) {
            json.append(commands.get(i).toJson());
            if (i < commands.size() - 1) {
                json.append(",");
            }
        }

        json.append("}");

        return json.toString();
    }

    /**
     * Turn light on.
     *
     * @return this object for chaining calls
     */
    public StateUpdate turnOn() {
        return setOn(true);
    }

    /**
     * Turn light off.
     *
     * @return this object for chaining calls
     */
    public StateUpdate turnOff() {
        return setOn(false);
    }

    private void addCommand(Command x) {
        if (commands.contains(x)) {
            return;
        } else {
            commands.add(x);
        }
    }

    /**
     * Turn light on or off.
     *
     * @param on on if true, off otherwise
     * @return this object for chaining calls
     */
    public StateUpdate setOn(boolean on) {
        addCommand(new Command("powered", on));
        if (!on) {
            addCommand(new Command("brightness", 0.0));
        }
        return this;
    }

    /**
     * Set brightness of light.
     * Brightness 0 is not the same as off.
     *
     * @param brightness brightness [1..254]
     * @return this object for chaining calls
     */
    public StateUpdate setBrightness(int brightness) {
        if (brightness < 1 || brightness > 254) {
            throw new IllegalArgumentException("Brightness out of range");
        }

        addCommand(new Command("brightness", brightness / 100.0));
        return this;
    }

    /**
     * Switch to HS color mode and set hue.
     *
     * @param hue hue [0..65535]
     * @return this object for chaining calls
     */
    public StateUpdate setHue(int hue) {
        if (hue < 0 || hue > 65535) {
            throw new IllegalArgumentException("Hue out of range");
        }

        addCommand(new Command("color_mode", "hsb"));
        addCommand(new Command("hue", hue / 65535.0));
        return this;
    }

    /**
     * Switch to HS color mode and set saturation.
     *
     * @param saturation saturation [0..254]
     * @return this object for chaining calls
     */
    public StateUpdate setSat(int saturation) {
        if (saturation < 0 || saturation > 254) {
            throw new IllegalArgumentException("Saturation out of range");
        }

        addCommand(new Command("color_mode", "hsb"));
        addCommand(new Command("saturation", saturation / 254.0));
        return this;
    }

    /**
     * Switch to XY color mode and set CIE color space coordinates.
     *
     * @param x x coordinate [0..1]
     * @param y y coordinate [0..1]
     * @return this object for chaining calls
     */
    public StateUpdate setXY(float x, float y) {
        return setXY(new float[] { x, y });
    }

    /**
     * Switch to XY color mode and set CIE color space coordinates.
     *
     * @param xy x and y coordinates [0..1, 0..1]
     * @return this object for chaining calls
     */
    public StateUpdate setXY(float[] xy) {
        if (xy.length != 2) {
            throw new IllegalArgumentException("Invalid coordinate array given");
        } else if (xy[0] < 0.0f || xy[0] > 1.0f || xy[1] < 0.0f || xy[1] > 1.0f) {
            throw new IllegalArgumentException("X and/or Y coordinate(s) out of bounds");
        }

        addCommand(new Command("color_mode", "xy"));
        addCommand(new Command("color_x", xy[0]));
        addCommand(new Command("color_y", xy[1]));
        return this;
    }

    /**
     * Switch to CT color mode and set color temperature in mired.
     *
     * @param colorTemperature color temperature [153..500]
     * @return this object for chaining calls
     */
    public StateUpdate setColorTemperature(int colorTemperature) {
        if (colorTemperature < 200 || colorTemperature > 650) {
            throw new IllegalArgumentException("Color temperature out of range");
        }

        addCommand(new Command("color_mode", "color_temperature"));
        addCommand(new Command("color_temperature", colorTemperature * 10));
        return this;
    }
}
